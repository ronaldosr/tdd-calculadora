package br.com.ronaldosr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class CalculadoraTeste {

    @Test
    public void testarSoma() {
        BigDecimal resultado = Calculadora.somar(3.4, 2.1);
        Assertions.assertEquals(BigDecimal.valueOf(5.5), resultado);
    }

    @Test
    public void testarSubtracao() {
        BigDecimal resultado = Calculadora.subtrair(3.4, 2.1);
        Assertions.assertEquals(BigDecimal.valueOf(1.3), resultado);
    }

    @Test
    public void testarMultiplicacao() {
        BigDecimal resultado = Calculadora.multiplicar(3.4, 2.1);
        Assertions.assertEquals(BigDecimal.valueOf(7.14), resultado);
    }

    @Test
    public void testarDivisaoPorDivedendoDiferenteZero() {
        BigDecimal resultado = Calculadora.dividir(3.4, 2.1);
        Assertions.assertEquals(new BigDecimal(1.62, new MathContext(3, RoundingMode.HALF_EVEN)), resultado);
    }

    @Test
    public void testarDivisaoPorDivedendoIgualZero() {
        Assertions.assertThrows(RuntimeException.class, () -> {
            Calculadora.dividir(3.4, 0.0);
        });
    }

}
