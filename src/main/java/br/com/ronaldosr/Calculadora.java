package br.com.ronaldosr;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.function.BiFunction;

public class Calculadora {
    public static BigDecimal somar(double operando1, double operando2) {
        return BigDecimal.valueOf(operando1).add(BigDecimal.valueOf(operando2));
    }

    public static BigDecimal subtrair(double operando1, double operando2) {
        return BigDecimal.valueOf(operando1).subtract(BigDecimal.valueOf(operando2));
    }

    public static BigDecimal multiplicar(double operando1, double operando2) {
        return BigDecimal.valueOf(operando1).multiply(BigDecimal.valueOf(operando2));
    }

    public static BigDecimal dividir(double operando1, double operando2) {
        if (BigDecimal.valueOf(operando2).equals(BigDecimal.valueOf(0.0))) {
            throw new RuntimeException("Não é possível dividor por zero");
        }
        return new BigDecimal(operando1, new MathContext(3, RoundingMode.HALF_EVEN))
                .divide(new BigDecimal(operando2, new MathContext(3, RoundingMode.HALF_EVEN)),
                        new MathContext(3, RoundingMode.HALF_EVEN));
    }
}
